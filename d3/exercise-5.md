## Exercise-5
### Solution
```
double t_exp(double x) {
  double tx;
  tx = fm_exp_t[6];
  tx = tx * x + fm_exp_t[5];
  tx = tx * x + fm_exp_t[4];
  tx = tx * x + fm_exp_t[3];
  tx = tx * x + fm_exp_t[2];
  tx = tx * x + fm_exp_t[1];
  tx = tx * x + fm_exp_t[0];
  tx = tx * x + fm_exp_t[0];
  return tx;
}
```
* From this code, it is clear that the degree of the Taylor expantion is 7.

```
double pad_exp(double x) {
  double px, qx;

  px = pad_exp_p[0];
  qx = pad_exp_p[0];
  px = px * x + pad_exp_p[1];
  qx = -qx * x + pad_exp_p[1];
  px = px * x + pad_exp_p[2];
  qx = -qx * x + pad_exp_p[2];
  px = px * x + pad_exp_p[3];
  qx = -qx * x + pad_exp_p[3];
  px = px * x + pad_exp_p[4];
  qx = -qx * x + pad_exp_p[4];
  x = px / qx;
  return x;
}
```
* From this snippet of the code, it is clear that the degree for the Pade approximation used is 4,4 for the p and q polynommila respectively.

#### P.S: All the code are compiled and run in my laptop with specification given below. All the runs are done for parameter 'numreps=10' and repeated 10 times.
```
processor	: 7
vendor_id	: GenuineIntel
cpu family	: 6
model		: 142
model name	: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
stepping	: 10
microcode	: 0x70
cpu MHz		: 1600.054
cache size	: 8192 KB
physical id	: 0
siblings	: 8
core id		: 3
cpu cores	: 4
apicid		: 7
initial apicid	: 7
fpu		: yes
fpu_exception	: yes
cpuid level	: 22
wp		: yes
```

#### Task-1
* Checking the performance for Pade and Taylor approximation all the 
* Using the Pade0 folder codes approximation provided

```
time for                 exp():   1.6750us  numreps 10
time for               t_exp():   0.0500us  avgerr  19.954
time for             pad_exp():   0.0500us  avgerr  0.0062729
```
* Using the Taylor0 folder codes the performance
```
time for                 exp():   1.8000us  numreps 10
time for               t_exp():   0.0500us  avgerr  19.954
```
* We can observe that both the approximations are faster than the in-built function but has way more error than the in-built.

#### Task-2 Switching to a smaller domain of [-0.5:0.5]

* Taylor0 result:
```
time for                 exp():   0.2225us  numreps 10
time for               t_exp():   0.0475us  avgerr  8.61251e-09
```
* Pade0 Results:
```
time for                 exp():   0.2250us  numreps 10
time for               t_exp():   0.0475us  avgerr  8.61251e-09
time for             pad_exp():   0.0400us  avgerr  4.2368e-12
```
* One thing is very noticable that for pade approximation the error value is significantly lower than the taylor's. The timings a re comparable as before but pade seems to be relatively faster then Taylor.


#### Task-3
* Here we are tryint to exploit all the tricks that we learned during the course to make the `exp(x)` more accurate and perform faster. 
* From the Day1 we learned that the Pade approximation works the best in the neoighboring of 0 and on day2 we learned that we can use the log-base-2 to calculate the the integer values much faster as it allows to use the bitwise manipulations. We are careful about the value of `log2(e) = 1.44269504088896338700e0`. Now with a few more operation we can use the `2^x` value to approximate the `e^(x)`.

* Here I present the results of the benchmark of the code.

* In the range of `[-0.5:0.5]`
```
time for                 exp():   0.2075us  numreps 10
time for               t_exp():   0.0450us  avgerr  8.61251e-09
time for             pad_exp():   0.0375us  avgerr  4.2368e-12
time for              my_exp():   0.0750us  avgerr  3.40231e-16
```
* In the range of `[-10:10]`
```
time for                 exp():   0.2625us  numreps 10
time for               t_exp():   0.0425us  avgerr  794768
time for             pad_exp():   0.0400us  avgerr  19.9407
time for              my_exp():   0.0775us  avgerr  7.78053e-16
```
#### Remarks/ Conclusion:

* We can notice that out implimentation is very accurate compared to others in multple orders. 
* Taylor approximations come out as the fastest of all the codes but has the least accuracy.
* The in-built function takes the most amount of time.
* Looks like Pade has the second best accuracy and timewise also takes the second position.
* Our implimatation seems to have the best trade off between timing and accuracy.




