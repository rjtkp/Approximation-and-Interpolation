## ecercise-4

* Without using Newton-Raphson
```
time for             invsqrt():   0.0087us  numreps 10
time for             Q_rsqrt():   0.0053us  avgerr  0.0245249
```
* One Newton-Raphson iteration
```
time for             invsqrt():   0.0085us  numreps 10
time for             Q_rsqrt():   0.0052us  avgerr  0.000869925
```
* Two Newton-Raphson iterations
```
time for             invsqrt():   0.0091us  numreps 10
time for             Q_rsqrt():   0.0081us  avgerr  1.43572e-06
```
### Remarks

* With just only 2 iterations the accurecy is improved upto `10^(-6)`. 
* As the time cost is in the order of microsecs, we can't guarantee the speed up as it could very well be effected by any other proccess running in the system.
* By Newton-Raphson, the convergence rate is really high.
