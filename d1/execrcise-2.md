## Execrcise-2
* The provided code has been run with different `nmax` values, ranging from 100 to 4000 and the out put has been recorded. From the output the data for time for execution and error value was noted down.
* The time to compute the linear spline is smaller. With the increment of the `nmax` value the time required is also inccresing because of grid refinement, which wasexpected. Below in the plot we can see the time teken by the built in and the approximating function for the execution wrt the nmax value.
![](https://github.com/rjtkp/Approximation-and-Interpolation/blob/master/d1/time_for_execution.png)

* As we know that we with better refinement we can approximate btter the error size is decresing with increse in the nmax value.
![](https://github.com/rjtkp/Approximation-and-Interpolation/blob/master/d1/error.png)
