# -*- Makefile -*-
SHELL=/bin/sh

default:
	$(MAKE) $(MFLAGS) -C d1
	$(MAKE) $(MFLAGS) -C d2
	# $(MAKE) $(MFLAGS) -C D3

clean:
	$(MAKE) $(MFLAGS) -C D1 clean
	$(MAKE) $(MFLAGS) -C D2 clean
	# $(MAKE) $(MFLAGS) -C D3 clean
